﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Bengkel_1.Models
{
    public class Employee
    {
        public Employee()
        {

        }

        [Required]
        public int ID { get; set; }

        [Required]
        [Display(Name ="First Name")]
        public string FName { get; set; }

        [Display(Name = "Last Name")]
        public string LName { get; set; }

        public string Department { get; set; }

        [Required]
        [Phone(ErrorMessage ="Sila masukkan format nombor telefon yang betul")]
        public string PhoneNo { get; set; }

        [EmailAddress(ErrorMessage ="Sila masukkan format email yang betul")]
        public string Email { get; set; }

        public static Employee GetEmployee(int id)
        {
            Employee emp = new Employee
            {
                ID = Convert.ToInt32(id),
                FName = "John",
                LName = "MacGyver",
                Department = "Human Resource",
                PhoneNo = "017-1234567",
                Email = "manjacat@youtube.com",
            };
            return emp;
        }

        public static IEnumerable<Employee> GetEmployees()
        {
            List<Employee> results = new List<Employee>();

            for (int i = 0; i < 10; i++)
            {
                Employee emp = Employee.GetEmployee(i);
                results.Add(emp);
            }

            return results;
        }       
    
    }

}
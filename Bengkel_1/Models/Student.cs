﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Bengkel_1.Models
{
    public class Student
    {
        public Student()
        {
            
        }

        [Required]
        public int ID { get; set; }

        [Required]
        public string FName { get; set; }
        public string LName { get; set; }

        public static Student GetStudent(int id)
        {
            Student st = new Student
            {
                ID = id,
                FName = "Ahmad",
                LName = "Maslan"
            };

            return st;
        }
    }
}
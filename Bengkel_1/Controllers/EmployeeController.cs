﻿using Bengkel_1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bengkel_1.Controllers
{
    public class EmployeeController : Controller
    {
        // GET: Employee
        public ActionResult Index()
        {
            IEnumerable<Employee> emps = Employee.GetEmployees();
            return View(emps);
        }

        public ActionResult Details(string id)
        {
            int idNo = Convert.ToInt32(id);
            Employee emp = Employee.GetEmployee(idNo);
            return View(emp);
        }

        public ActionResult Edit(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return RedirectToAction("Index");
            }
            int idNo = Convert.ToInt32(id);
            Employee emp = Employee.GetEmployee(idNo);
            return View(emp);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Employee emp)
        {
            if (ModelState.IsValid)
            {
                //Save Changes
                ViewBag.Message = string.Format("Edited emp-{0} successfully.", emp.ID);
            }
            return View(emp);
        }
    }
}
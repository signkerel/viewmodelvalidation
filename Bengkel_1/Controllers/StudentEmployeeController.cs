﻿using Bengkel_1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bengkel_1.Controllers
{
    public class StudentEmployeeController : Controller
    {
        // GET: StudentEmployee
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Edit(string id, string msg)
        {
            if (!string.IsNullOrEmpty(msg))
            {
                
            }
            int idNo = Convert.ToInt32(id);
            StudentEmployee student = StudentEmployee.GetStudentEmployee(idNo);
            return View(student);
        }

        [HttpPost]
        public ActionResult Edit(StudentEmployee st)
        {
            //ModelState.Remove("Employee.Email");
            if (ModelState.IsValid)
            {
                ViewBag.Message = "No Error";
            }
            return View(st);
        }

    }
}
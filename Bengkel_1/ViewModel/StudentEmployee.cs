﻿using Bengkel_1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bengkel_1.ViewModel
{
    public class StudentEmployee
    {
        public StudentEmployee()
        {

        }

        public Employee Employee { get; set; }
        public Student Student { get; set; }

        public static StudentEmployee GetStudentEmployee(int id)
        {
            StudentEmployee st = new StudentEmployee
            {
                Employee = Employee.GetEmployee(id),
                Student = Student.GetStudent(id)
            };
            return st;
        }
    }
}